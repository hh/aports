# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=py3-elementpath
pkgver=4.1.0
pkgrel=0
pkgdesc="XPath 1.0/2.0 parsers and selectors for ElementTree and lxml"
url="https://github.com/sissaschool/elementpath"
arch="noarch"
license="MIT"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-lxml"
source="$pkgname-$pkgver.tar.gz::https://github.com/sissaschool/elementpath/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/elementpath-$pkgver/"

build() {
	python3 setup.py build
}

check() {
	# first four disabled tests fails on locale differences
	# Rest of the tests need more investigation, started with upgrade to 4.1.0
	pytest \
		--deselect tests/test_xpath2_functions.py::XPath2FunctionsTest::test_compare_function \
		--deselect tests/test_xpath30.py::XPath30FunctionsTest::test_compare_function \
		--deselect tests/test_xpath31.py::XPath31FunctionsTest::test_compare_function \
		--deselect tests/test_collations.py::CollationsTest::test_context_activation \
		--deselect tests/test_xpath2_functions.py::LxmlXPath2FunctionsTest::test_compare_function \
		--deselect tests/test_xpath30.py::LxmlXPath30FunctionsTest::test_compare_function \
		--deselect tests/test_xpath31.py::LxmlXPath31FunctionsTest::test_compare_function \
		--deselect tests/test_validators.py::ValidatorsTest::test_validate_analyzed_string \
		--deselect tests/test_validators.py::ValidatorsTest::test_validate_json_to_xml
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
8cc67475ab405065cf8cbbca9e479404177d03be48149aa4eaa4565cbc367ef87b6ba1c79772d02dbb2b50eed4ec628844c2bab7f4c65c44e64b9ea93a5da748  py3-elementpath-4.1.0.tar.gz
"
