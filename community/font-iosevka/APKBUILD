# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=22.0.1
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
7ac7cdf899ff829637d532733f7ec16111f849d1df538262d8a6295c8ae1d6aac9ca745f1f8676d8d76d87cbbe0c8737c0a3ed0beaf0c0fb0ae4f042d063f0c7  super-ttc-iosevka-22.0.1.zip
33510c639523d6d47ca1aa07bfbe7a10319d88025c0d0fac1b47923e5b0b82bfce0c2eeb497e2086faeb1aefab5fb760f5dac9eefafd4c187034f4f9f7dc667d  super-ttc-iosevka-aile-22.0.1.zip
2982fed1b4502e1131f773752d5f086427839cae221e4f6a3e35efd50a7e96aad3121d795d7e1a42d5f06df90012e3ddf54b8cdd39613e24d0aa036c05832d37  super-ttc-iosevka-slab-22.0.1.zip
90f42da4c538811343965baf1136259c13cd8b6e528422634811a40eb0f4367d86e07ba92021e833e7c3174c6dae672f4ae147e2dad1fc3854f8ad4b6d46ad67  super-ttc-iosevka-curly-22.0.1.zip
3aee79c143b2fbf87e8e445a68cb79470077f67cf4d23bbc5e165596ec865b2cc40d030050d8ee41dda2fec22ad6338c6cbc32b00e12be45578a2026d150fdc1  super-ttc-iosevka-curly-slab-22.0.1.zip
"
