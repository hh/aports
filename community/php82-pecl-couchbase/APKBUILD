# Contributor: Nathan Johnson <nathan@nathanjohnson.info>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php82-pecl-couchbase
_extname=couchbase
pkgver=4.1.2
pkgrel=0
pkgdesc="PHP 8.2 extension for Couchbase - PECL"
url="https://pecl.php.net/package/couchbase"
arch="all !riscv64" # ftbfs
license="Apache-2.0"
_phpv=82
_php=php$_phpv
depends="$_php-pecl-igbinary"
makedepends="$_php-dev openssl-dev>3 linux-headers cmake chrpath"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"

build() {
	phpize$_phpv
	./configure --prefix=/usr --with-php-config=php-config$_phpv
	make
}

check() {
	# no tests shipped via PECL
	$_php -d extension="$builddir"/modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	chrpath -r "/usr/lib/$_php/modules" "$pkgdir"/usr/lib/$_php/modules/$_extname.so
	chrpath -d "$pkgdir"/usr/lib/$_php/modules/libcouchbase_php_wrapper.so

	local _confdir="$pkgdir"/etc/$_php/conf.d
	mkdir -p $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="
16dea228794b9dee9178577c47c40f9abd83708ff387d44aabbeb82aa722bfe4c16e0e257a08bcc0c942b2c07511781f20236cf2a195b62fe9699b04f6bee83b  php-pecl-couchbase-4.1.2.tgz
"
